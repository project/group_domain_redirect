[[_TOC_]]

#### Introduction

The domain redirection module, as its name suggests, redirects a group
to a domain. This domain functionality already exists in the Domain
module, on which this module depends. The difference lies in that by
this module when clicking directly on the group link or in the group
view option, it redirects us to the selected domain, that is to say,
directly from the current site we can access the other domain through
the group links.

#### Requirements

Module requirements:
- The Groups and Domain module must be active and previously configured.
- It depends on the module Domain group

#### Maintainers

Seed Em:
https://www.drupal.org/seed-em
