<?php

namespace Drupal\group_domain_redirect\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\domain\Entity\Domain;

/**
 * Class Group_Domain_Redirect.
 */
class GroupDomainRedirect implements EventSubscriberInterface {

  /**
   * The groupDomainRedirect.
   */
  public function __construct(GetResponseEvent $event) {
    $request = $event->getRequest();

    // Check if the route is that of a group.
    if ($request->attributes->get('_route') !== 'entity.group.canonical') {
      return;
    }
    // Gets the current hostname.
    $host = $request->getHost();
    // Gets the group ID.
    $id = $request->attributes->get('group')->id();
    // The information for the domain of this group is loaded.
    $domain = Domain::load('group_' . $id);
    // Only if there is an associated domain.
    if ($domain) {
      $hostname = $domain->getHostname();
      // As long as the hostname of the request is
      // different from the redirection.
      if ($host != $hostname) {
        $response = new TrustedRedirectResponse('http://' . $hostname);
        $event->setResponse($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['groupDomainRedirect', 30];
    return $events;
  }
}
